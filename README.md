# Docker in a VirtualBox

Simple alternative to Docker Desktop

# Security

At the moment there are no certificates in place when communicating with remote Docker, which causes it to complain with:
```
WARNING: API is accessible on http://0.0.0.0:2375 without encryption.
         Access to the remote API is equivalent to root access on the host. Refer
         to the 'Docker daemon attack surface' section in the documentation for
         more information: https://docs.docker.com/go/attack-surface/
```

As this is a local installation of Docker this shouldn't cause too much issues.
The certificates will be added at a later time.


# Requirements

1. Packer - get it at: [packer.io](https://www.packer.io/downloads)
2. VirtualBox - get it at: [oracle.com](https://www.oracle.com/uk/virtualization/technologies/vm/downloads/virtualbox-downloads.html)
3. Docker client - get it at: [docker.com](https://docs.docker.com/engine/install/binaries/#install-client-binaries-on-macos)

# Build it

Start the build process:

```
$ packer build docker.pkr.hcl
```

This should take a couple of minutes. The process will download Alpine Linux distribution and build a VirtualBox image with Docker.

# Run it

Packer will create two files in the output directory (most likely the name of the directory is: output-docker):
- OVF file containing the image definition
- VMDK file containing the actual image

Import the OVF file in your VirtualBox and start the VM.

# Use it

Docker client needs to be told to a non-default address when communicating with the daemon:
```
export DOCKER_HOST="tcp://127.0.0.1"
```

Follow the instructions of your shell to make this variable persist across restarts.


# Use kind

To be able to communicate with the cluster, it needs to be told not to listen on localhost and the latest kind nodes have some issues with Alpine Linux. In this directory you can find a [cluster configuration file](kind.yaml) which can be used to create a working kind cluster:
```
$ kind create cluster --config kind.yaml
```
