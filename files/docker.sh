#!/bin/sh

set -x
set -euo pipefail

apk add docker
rc-update add docker default

mkdir -p /etc/docker
cat > /etc/docker/daemon.json <<EOF
{
  "debug": false,
  "tls": false,
  "hosts": [
    "unix://var/run/docker.sock",
    "tcp://0.0.0.0"
  ]
}
EOF
