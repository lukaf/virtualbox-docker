#!/bin/sh

set -x
set -euo pipefail

cat > /etc/apk/repositories <<EOF
https://uk.alpinelinux.org/alpine/v${ALPINE_RELEASE}/main
https://uk.alpinelinux.org/alpine/v${ALPINE_RELEASE}/community
EOF

setup-keymap us us
setup-hostname -n "${HOSTNAME}"
setup-interfaces -i <<EOF
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp
      hostname ${HOSTNAME}
EOF
setup-dns -d "." 9.9.9.9
setup-timezone -z UTC
setup-ntp -c chrony
yes | setup-disk -v -m sys /dev/sda || true

reboot
