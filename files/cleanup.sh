#!/bin/sh

set -x
set -euo pipefail

if [ -f /etc/ssh/sshd_config.orig ]; then
    cp /etc/ssh/sshd_config.orig /etc/ssh/sshd_config
fi
