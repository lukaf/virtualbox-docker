variable "alpine_release" {
  type = string
  default = "3.14"
}

variable "alpine_revision" {
  type = number
  default = 2
}

variable "hostname" {
  type = string
  default = "docker"
}

variable "memory" {
  type = number
  default = 4
  description = "Memory in GB"
}

variable "cpus" {
  type = number
  default = 4
  description = "Number of CPUs"
}

variable "storage" {
  type = number
  default = 50
  description = "Storage size in GB"
}

packer {
  required_plugins {
    virtualbox = {
      version = ">= 0.0.1"
      source = "github.com/hashicorp/virtualbox"
    }
  }
}

source "virtualbox-iso" "docker" {
  headless = true

  guest_os_type = "Linux26_64"

  iso_url = "https://dl-cdn.alpinelinux.org/alpine/v${var.alpine_release}/releases/x86_64/alpine-virt-${var.alpine_release}.${var.alpine_revision}-x86_64.iso"
  iso_checksum = "file:https://dl-cdn.alpinelinux.org/alpine/v${var.alpine_release}/releases/x86_64/alpine-virt-${var.alpine_release}.${var.alpine_revision}-x86_64.iso.sha256"

  ssh_username = "root"
  ssh_password = ""

  shutdown_command = "poweroff"

  boot_wait = "20s"
  boot_command = [
    "root<enter><wait>",
    "ip link set eth0 up<enter>",
    "udhcpc -i eth0<enter><wait>",
    "setup-sshd -c openssh<enter><wait>",
    "cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig<enter>",
    "echo 'PermitRootLogin yes' >> /etc/ssh/sshd_config<enter>",
    "echo 'PermitEmptyPasswords yes' >> /etc/ssh/sshd_config<enter>",
    "rc-service sshd restart<enter>"
  ]

  disk_size = 1024 * var.storage
  cpus = var.cpus
  memory = 1024 * var.memory
  gfx_controller = "vmsvga"

  vboxmanage_post = [
    ["modifyvm", "{{.Name}}", "--natpf1", "docker,tcp,127.0.0.1,2375,,2375"],
    ["modifyvm", "{{.Name}}", "--natpf1", "kind,tcp,127.0.0.1,6443,,6443"],
    ["modifyvm", "{{.Name}}", "--vrde", "off"]
  ]
}

build {
  name = "docker"
  sources = ["sources.virtualbox-iso.docker"]

  provisioner "shell" {
    expect_disconnect = true
    script = "files/setup.sh"
    environment_vars = [
      "ALPINE_RELEASE=${var.alpine_release}",
      "HOSTNAME=${var.hostname}"
    ]
  }

  provisioner "shell" {
    pause_before = "10s"
    script = "files/docker.sh"
  }

  provisioner "shell" {
    script = "files/cleanup.sh"
  }
}
